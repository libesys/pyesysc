/*!
 * \file pysimple_bus/pysimple_bus_prec.cpp
 * \brief For precompiled headers
 *
 * \cond
 *__legal_b__
 *
 *__legal_e__
 * \endcond
 *
 */

// pysimple_bus.cpp : source file that includes just the standard includes
// pysimple_bus.pch will be the pre-compiled header
// pysimple_bus.obj will contain the pre-compiled type information

#include "pysimple_bus/pysimple_bus_prec.h"

// TODO: reference any additional headers you need in pysimple_bus_prec.h
// and not in this file

