%module(directors="1") simple_bus
%include typemaps.i
%include std_string.i
%include std_vector.i
%include stdint.i
%{

#include <stdio.h>

#include "pysimple_bus/pysimple_bus_defs.h"


%}

#define SC_API
#define PYSIMPLE_BUS_API
#define SIMPLE_BUS_API

%include simple_bus_arbiter_if.hh
%include simple_bus_blocking_if.hh
%include simple_bus_direct_if.hh
%include simple_bus_non_blocking_if.hh
%include simple_bus_slave_if.hh
%include simple_bus_arbiter.hh
%include simple_bus_test.hh
%include simple_bus.hh
%include simple_bus_fast_mem.hh
%include simple_bus_slow_mem.hh
%include simple_bus_master_blocking.hh
%include simple_bus_master_direct.hh
%include simple_bus_master_non_blocking.hh
%include pyversion.hh
