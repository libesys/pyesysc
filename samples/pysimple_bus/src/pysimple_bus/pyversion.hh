/*!
 * \file pysimple_bus/pyversion.h
 * \brief Version info for pysimple_bus
 *
 * \cond
 *__legal_b__
 *
 *__legal_e__
 * \endcond
 *
 */

#pragma once

// Bump-up with each new version
#define PYSIMPLE_BUS_MAJOR_VERSION    0
#define PYSIMPLE_BUS_MINOR_VERSION    0
#define PYSIMPLE_BUS_RELEASE_NUMBER   1
#define PYSIMPLE_BUS_VERSION_STRING   _T("pysimple_bus 0.0.1")

// Must be updated manually as well each time the version above changes
#define PYSIMPLE_BUS_VERSION_NUM_DOT_STRING   "0.0.1"
#define PYSIMPLE_BUS_VERSION_NUM_STRING       "0001"

// nothing should be updated below this line when updating the version

#define PYSIMPLE_BUS_VERSION_NUMBER (PYSIMPLE_BUS_MAJOR_VERSION * 1000) + (PYSIMPLE_BUS_MINOR_VERSION * 100) + PYSIMPLE_BUS_RELEASE_NUMBER
#define PYSIMPLE_BUS_BETA_NUMBER      1
#define PYSIMPLE_BUS_VERSION_FLOAT PYSIMPLE_BUS_MAJOR_VERSION + (PYSIMPLE_BUS_MINOR_VERSION/10.0) + (PYSIMPLE_BUS_RELEASE_NUMBER/100.0) + (PYSIMPLE_BUS_BETA_NUMBER/10000.0)

// check if the current version is at least major.minor.release
#define PYSIMPLE_BUS_CHECK_VERSION(major,minor,release) \
    (PYSIMPLE_BUS_MAJOR_VERSION > (major) || \
    (PYSIMPLE_BUS_MAJOR_VERSION == (major) && PYSIMPLE_BUS_MINOR_VERSION > (minor)) || \
    (PYSIMPLE_BUS_MAJOR_VERSION == (major) && PYSIMPLE_BUS_MINOR_VERSION == (minor) && PYSIMPLE_BUS_RELEASE_NUMBER >= (release)))

