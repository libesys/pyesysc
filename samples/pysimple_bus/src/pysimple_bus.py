import os
import sys
import errno

if "PYSWIG" in os.environ:
    pyswig_dir=os.environ.get("PYSWIG")
    print("pyswig_dir = %s" % pyswig_dir)
    sys.path.append(pyswig_dir)

import pyswig

esysc_dir = None

if "ESYS_SYSC" in os.environ:
    esysc_dir = os.environ.get("ESYS_SYSC")
else:
    print("ERROR : ESYS_SYSC not found in environment variables.")
    sys.exit(errno.ENOENT)

# the base directory where the source file can be found
source_dir = "%s/examples/sysc/simple_bus" % esysc_dir
output_dir = "./pysimple_bus"
# path to all sources, but relative to the base directory given above (including the directory structure
source=["simple_bus_arbiter_if.h",
        "simple_bus_blocking_if.h",
        "simple_bus_direct_if.h",
        "simple_bus_non_blocking_if.h",
        "simple_bus_slave_if.h",

        "simple_bus_arbiter.h",
        "simple_bus_test.h",
        "simple_bus.h",
        "simple_bus_fast_mem.h",
        "simple_bus_slow_mem.h",
        "simple_bus_master_blocking.h",
        "simple_bus_master_direct.h",
        "simple_bus_master_non_blocking.h",
        ]

print("pysimple_bus.py begins...")

options = pyswig.pyswig_options()
options.SetInputDir(source_dir)
options.SetOutputDir(output_dir)
options.SetOutputFileExt("hh")
options.SetSourceFiles(source)

obj = pyswig.pyswig(options)
obj.do()

# the base directory where the source file can be found
source_extra_dir = "../include/pysimple_bus"
output_extra_dir = "./pysimple_bus"
# path to all sources, but relative to the base directory given above (including the directory structure
source_extra = ["pyversion.h"
                ]
options_extra = pyswig.pyswig_options()
options_extra.SetInputDir(source_extra_dir)
options_extra.SetOutputDir(output_extra_dir)
options_extra.SetOutputFileExt("hh")
options_extra.SetSourceFiles(source_extra)

obj_extra = pyswig.pyswig(options_extra)
obj_extra.do()

fout = open(output_dir+os.sep+"pysimple_bus."+"hh", "w")
fout.write("%module(directors=\"1\") simple_bus\n")
fout.write("%include typemaps.i\n")
fout.write("%include std_string.i\n")
# fout.write("%include std_wstring.i\n")
fout.write("%include std_vector.i\n")
fout.write("%include stdint.i\n")
# fout.write("%include my_typemaps.i\n")
fout.write("%{\n")
fout.write("\n")
fout.write("#include <stdio.h>\n")
fout.write("\n")
#fout.write("#include \"systemc.h\"\n")
fout.write("#include \"pysimple_bus/pysimple_bus_defs.h\"\n\n")
#fout.write("#ifdef ERROR\n")
#fout.write("#undef ERROR\n")
#fout.write("#endif\n\n")
fout.write("\n")
fout.write("%}\n\n")
fout.write("#define SC_API\n")
fout.write("#define PYSIMPLE_BUS_API\n")
fout.write("#define SIMPLE_BUS_API\n")
#fout.write("#define URLOG_USE_STD_CPP\n")
fout.write("\n")


for f in source:
    # if os.path.isdir(f)==False:
    fout.write("%%include %sh\n" % f)

for f in source_extra:
    # if os.path.isdir(f)==False:
    fout.write("%%include %sh\n" % f)

fout.close()
output_dir="../include/pysimple_bus"
fout = open(output_dir+os.sep+"pysimple_bus."+"h", "w")
fout.write("#pragma once\n")
fout.write("\n")

for f in source:
    # if os.path.isdir(f)==False:
    fout.write("#include \"urlog/%s\"\n" %f)

for f in source_extra:
    # if os.path.isdir(f)==False:
    fout.write("#include \"pysimple_bus/%s\"\n" %f)

fout.write("\n\n")
fout.close()

print("pysimple_bus.py ends.")

