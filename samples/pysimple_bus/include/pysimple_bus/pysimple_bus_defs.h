/*!
 * \file pysimple_bus/pysimple_bus_defs.h
 * \brief Definitions needed for pysimple_bus
 *
 * \cond
 *__legal_b__
 *
 *__legal_e__
 * \endcond
 *
 */

#pragma once

#ifdef PYSIMPLE_BUS_EXPORTS
#define PYSIMPLE_BUS_API __declspec(dllexport)
#elif PYSIMPLE_BUS_USE
#define PYSIMPLE_BUS_API __declspec(dllimport)
#else
#define PYSIMPLE_BUS_API
#endif
