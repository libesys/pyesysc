/*!
 * \file pyesysc/pyesysc_defs.h
 * \brief Definitions needed for pyesysc
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#ifdef PYESYSC_EXPORTS
#define PYESYSC_API __declspec(dllexport)
#elif PYESYSC_USE
#define PYESYSC_API __declspec(dllimport)
#else
#define PYESYSC_API
#endif
