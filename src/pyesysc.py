import os
import sys

if os.environ.get("PYSWIG"):
    pyswig_dir=os.environ.get("PYSWIG")
    print("pyswig_dir = %s" % pyswig_dir)
    sys.path.append(pyswig_dir)

import pyswig

# the base directory where the source file can be found
source_dir = "../../esysc/src"
output_dir = "./pyesysc"
# path to all sources, but relative to the base directory given above (including the directory structure
source=["sysc/version.h",
        "sysc/datatypes/int/sc_nbdefs.h",

        "sysc/kernel/sc_attribute.h",
        "sysc/kernel/sc_object.h",
        "sysc/kernel/sc_module_name.h",
        "sysc/communication/sc_interface.h",
        "sysc/kernel/sc_process.h",
        "sysc/kernel/sc_module.h",
        "sysc/kernel/sc_time.h",
        "sysc/kernel/sc_status.h",
        "sysc/utils/sc_report.h",
        "sysc/kernel/sc_simcontext.h",
        "sysc/kernel/sc_event.h",
        "sysc/kernel/sc_wait.h",
        "sysc/kernel/sc_ver.h",

        "sysc/kernel/sc_sensitive.h",
        "sysc/kernel/sc_module_registry.h",
        "sysc/kernel/sc_name_gen.h",
        "sysc/kernel/sc_object_manager.h",
        "sysc/kernel/sc_runnable.h",
        "sysc/kernel/sc_wait_cthread.h",

        "sysc/utils/sc_typeindex.h",
        "sysc/communication/sc_port.h",
        "sysc/communication/sc_signal_ifs.h",
        "sysc/communication/sc_signal_ports.h",
        "sysc/communication/sc_clock_ports.h",
        "sysc/communication/sc_clock.h",
        "sysc/communication/sc_prim_channel.h",

        "sysc/datatypes/bit/sc_bit.h",
        "sysc/datatypes/bit/sc_logic.h",
        "sysc/communication/sc_signal.h",
        "sysc/communication/sc_mutex_if.h",
        "sysc/communication/sc_mutex.h",
        "sysc/communication/sc_buffer.h",
        # "communication/sc_event_finder.h",
        "sysc/communication/sc_fifo_ifs.h",
        "sysc/communication/sc_fifo.h",
        "sysc/communication/sc_fifo_ports.h",
        "sysc/communication/sc_semaphore_if.h",
        "sysc/communication/sc_semaphore.h",
        "sysc/communication/sc_signal_resolved.h",
        "sysc/communication/sc_signal_resolved_ports.h",

        "sysc/utils/sc_string.h",
        "sysc/utils/sc_stop_here.h",
        "sysc/utils/sc_utils_ids.h",
        "sysc/utils/sc_vector.h",

        "sysc/tracing/sc_trace.h",
        
        "sysc/sc_simulation.h",
        
        "systemc.h",
        ]

print("pyesysc.py begins...")

options = pyswig.pyswig_options()
options.SetInputDir(source_dir)
options.SetOutputDir(output_dir)
options.SetOutputFileExt("hh")
options.SetSourceFiles(source)

obj = pyswig.pyswig(options)
obj.do()

# the base directory where the source file can be found
source_extra_dir = "../include/pyesysc"
output_extra_dir = "./pyesysc"
# path to all sources, but relative to the base directory given above (including the directory structure
source_extra = ["pyversion.h"
                ]
options_extra = pyswig.pyswig_options()
options_extra.SetInputDir(source_extra_dir)
options_extra.SetOutputDir(output_extra_dir)
options_extra.SetOutputFileExt("hh")
options_extra.SetSourceFiles(source_extra)

obj_extra = pyswig.pyswig(options_extra)
obj_extra.do()

fout = open(output_dir+os.sep+"esysc."+"hh", "w")
fout.write("%module(directors=\"1\") esysc\n")
fout.write("%include typemaps.i\n")
fout.write("%include std_string.i\n")
# fout.write("%include std_wstring.i\n")
fout.write("%include std_vector.i\n")
fout.write("%include stdint.i\n")
# fout.write("%include my_typemaps.i\n")
fout.write("%{\n")
fout.write("\n")
fout.write("#include <stdio.h>\n")
fout.write("\n")
#fout.write("#include \"systemc.h\"\n")
fout.write("#include \"pyesysc/pyesysc_defs.h\"\n\n")
#fout.write("#ifdef ERROR\n")
#fout.write("#undef ERROR\n")
#fout.write("#endif\n\n")
fout.write("\n")
fout.write("%}\n\n")
fout.write("#define SC_API\n")
fout.write("#define PYESYSC_API\n")
#fout.write("#define URLOG_USE_STD_CPP\n")
fout.write("\n")


for f in source:
    # if os.path.isdir(f)==False:
    fout.write("%%include %sh\n" % f)

for f in source_extra:
    # if os.path.isdir(f)==False:
    fout.write("%%include %sh\n" % f)

fout.close()
output_dir="../include/pyesysc"
fout = open(output_dir+os.sep+"pyesysc."+"h", "w")
fout.write("#pragma once\n")
fout.write("\n")

for f in source:
    # if os.path.isdir(f)==False:
    fout.write("#include \"urlog/%s\"\n" %f)

for f in source_extra:
    # if os.path.isdir(f)==False:
    fout.write("#include \"pyesysc/%s\"\n" %f)

fout.write("\n\n")
fout.close()

print("pyesysc.py ends.")

