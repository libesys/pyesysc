/*!
 * \file sysc/sc_simulation.h
 * \brief Defines the class replacing sc_main
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2008-2018 Michel Gillet
 * Distributed under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "sysc/kernel/sc_cmnhdr.h"

//<swig>
%{
#include "sysc/sc_simulation.h"
%}
//</swig>

namespace sc_core
{

%feature("director") sc_simulation;
/*! \class sc_simulation sysc/sc_simulation.h "sysc/sc_simulation.h"
 *  \brief The main class creating a SystemC simulation
 */
class SC_API sc_simulation
{
public:
    typedef int(*sc_main_with_param_type)(int, char **);
	typedef int(*sc_main_with_param_simul)(sc_simulation *);

    /// Constructor
    sc_simulation();

    sc_simulation(sc_main_with_param_type main_with_param);

    /// Destructor
    virtual ~sc_simulation();

    /// The function to call
    virtual int run();
    /// The main function to overload to define a simulation
    virtual int main();

    /// Reset a simulation
    void reset();

	void set_argv_call(int argc, char* argv[]);

    /// Needed to initialize the simulator
    static int init();
    /// Various clean up after a simulation
    static int clean();

    /// Return a pointer to the sc_action boolean
    static bool *get_sc_in_action();

    static bool is_cleaned();
    static bool is_init();
protected:
    static bool m_init;
    static bool m_cleaned;

	int m_argc = 0;
	char **m_argv = nullptr;

    sc_main_with_param_type m_main_with_param = nullptr;
	sc_main_with_param_simul m_main_with_param_simul = nullptr;

	std::vector<std::string> m_argv_call;
};

}

#ifdef USE_SC_SIMULATION
#define SC_SIMULATION() \
int main(int argc, char* argv[]) \
{ \
    sc_simulation my_simulation(&sc_main); \
\
    my_simulation.set_argv_call(argc, argv); \
\
    int result = sc_simulation::init(); \
    if (result<0) \
        return -result; \
    result = my_simulation.run(); \
\
    return result; \
} \
struct lalalalala_systemc {}

#define SC_SIMULATION_END() \
sc_simulation::clean()

#else
#define SC_SIMULATION() struct lalalalala_systemc {}
#define SC_SIMULATION_END() struct lalalalala1_systemc {}
#endif




