/*! 
 * \file esysc/version.h
 * \brief Version info about mySystemC
 * 
 *
 * __legal_b__
 *
 * __legal_e__
 *
 *
 */

#ifndef __MYSYSTEMC_VERSION_H__
#define __MYSYSTEMC_VERSION_H__

// Bump-up with each new version
#define MYSYSTEMC_MAJOR_VERSION    0
#define MYSYSTEMC_MINOR_VERSION    1
#define MYSYSTEMC_RELEASE_NUMBER   0
#define MYSYSTEMC_VERSION_STRING   _T("mySystemC 0.1.0")

// These are used by src/msw/version.rc and should always be ASCII, not Unicode
// and must be updated manually as well each time the version above changes
#define MYSYSTEMC_VERSION_NUM_DOT_STRING   "0.1.0"
#define MYSYSTEMC_VERSION_NUM_STRING       "000100"

// nothing should be updated below this line when updating the version

#define MYSYSTEMC_VERSION_NUMBER (MYSYSTEMC_MAJOR_VERSION * 1000) + (MYSYSTEMC_MINOR_VERSION * 100) + MYSYSTEMC_RELEASE_NUMBER
#define MYSYSTEMC_BETA_NUMBER      1
#define MYSYSTEMC_VERSION_FLOAT MYSYSTEMC_MAJOR_VERSION + (MYSYSTEMC_MINOR_VERSION/10.0) + (MYSYSTEMC_RELEASE_NUMBER/100.0) + (MYSYSTEMC_BETA_NUMBER/10000.0)

// check if the current version is at least major.minor.release
#define MYSYSTEMC_CHECK_VERSION(major,minor,release) \
    (MYSYSTEMC_MAJOR_VERSION > (major) || \
    (MYSYSTEMC_MAJOR_VERSION == (major) && MYSYSTEMC_MINOR_VERSION > (minor)) || \
    (MYSYSTEMC_MAJOR_VERSION == (major) && MYSYSTEMC_MINOR_VERSION == (minor) && MYSYSTEMC_RELEASE_NUMBER >= (release)))


#endif

