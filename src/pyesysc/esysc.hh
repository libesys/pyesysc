%module(directors="1") esysc
%include typemaps.i
%include std_string.i
%include std_vector.i
%include stdint.i
%{

#include <stdio.h>

#include "pyesysc/pyesysc_defs.h"


%}

#define SC_API
#define PYESYSC_API

%include sysc/version.hh
%include sysc/datatypes/int/sc_nbdefs.hh
%include sysc/kernel/sc_attribute.hh
%include sysc/kernel/sc_object.hh
%include sysc/kernel/sc_module_name.hh
%include sysc/communication/sc_interface.hh
%include sysc/kernel/sc_process.hh
%include sysc/kernel/sc_module.hh
%include sysc/kernel/sc_time.hh
%include sysc/kernel/sc_status.hh
%include sysc/utils/sc_report.hh
%include sysc/kernel/sc_simcontext.hh
%include sysc/kernel/sc_event.hh
%include sysc/kernel/sc_wait.hh
%include sysc/kernel/sc_ver.hh
%include sysc/kernel/sc_sensitive.hh
%include sysc/kernel/sc_module_registry.hh
%include sysc/kernel/sc_name_gen.hh
%include sysc/kernel/sc_object_manager.hh
%include sysc/kernel/sc_runnable.hh
%include sysc/kernel/sc_wait_cthread.hh
%include sysc/utils/sc_typeindex.hh
%include sysc/communication/sc_port.hh
%include sysc/communication/sc_signal_ifs.hh
%include sysc/communication/sc_signal_ports.hh
%include sysc/communication/sc_clock_ports.hh
%include sysc/communication/sc_clock.hh
%include sysc/communication/sc_prim_channel.hh
%include sysc/datatypes/bit/sc_bit.hh
%include sysc/datatypes/bit/sc_logic.hh
%include sysc/communication/sc_signal.hh
%include sysc/communication/sc_mutex_if.hh
%include sysc/communication/sc_mutex.hh
%include sysc/communication/sc_buffer.hh
%include sysc/communication/sc_fifo_ifs.hh
%include sysc/communication/sc_fifo.hh
%include sysc/communication/sc_fifo_ports.hh
%include sysc/communication/sc_semaphore_if.hh
%include sysc/communication/sc_semaphore.hh
%include sysc/communication/sc_signal_resolved.hh
%include sysc/communication/sc_signal_resolved_ports.hh
%include sysc/utils/sc_string.hh
%include sysc/utils/sc_stop_here.hh
%include sysc/utils/sc_utils_ids.hh
%include sysc/utils/sc_vector.hh
%include sysc/tracing/sc_trace.hh
%include sysc/sc_simulation.hh
%include systemc.hh
%include pyversion.hh
