/*!
 * \file pyesysc/version.h
 * \brief Version info for pyESysC
 *
 * \cond
 *__legal_b__
 *
 *__legal_e__
 * \endcond
 *
 */

#pragma once

// Bump-up with each new version
#define PYESYSC_MAJOR_VERSION    0
#define PYESYSC_MINOR_VERSION    0
#define PYESYSC_RELEASE_NUMBER   1
#define PYESYSC_VERSION_STRING   _T("pyESysC 0.0.1")

// Must be updated manually as well each time the version above changes
#define PYESYSC_VERSION_NUM_DOT_STRING   "0.0.1"
#define PYESYSC_VERSION_NUM_STRING       "0001"

// nothing should be updated below this line when updating the version

#define PYESYSC_VERSION_NUMBER (PYESYSC_MAJOR_VERSION * 1000) + (PYESYSC_MINOR_VERSION * 100) + PYESYSC_RELEASE_NUMBER
#define PYESYSC_BETA_NUMBER      1
#define PYESYSC_VERSION_FLOAT PYESYSC_MAJOR_VERSION + (PYESYSC_MINOR_VERSION/10.0) + (PYESYSC_RELEASE_NUMBER/100.0) + (PYESYSC_BETA_NUMBER/10000.0)

// check if the current version is at least major.minor.release
#define PYESYSC_CHECK_VERSION(major,minor,release) \
    (PYESYSC_MAJOR_VERSION > (major) || \
    (PYESYSC_MAJOR_VERSION == (major) && PYESYSC_MINOR_VERSION > (minor)) || \
    (PYESYSC_MAJOR_VERSION == (major) && PYESYSC_MINOR_VERSION == (minor) && PYESYSC_RELEASE_NUMBER >= (release)))



