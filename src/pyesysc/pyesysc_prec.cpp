/*!
 * \file pymysystemc/pymysystemc_prec.cpp
 * \brief For precompiled headers
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

// pymysystemc.cpp : source file that includes just the standard includes
// pymysystemc.pch will be the pre-compiled header
// pymysystemc.obj will contain the pre-compiled type information

#include "pyesysc/pyesysc_prec.h"

// TODO: reference any additional headers you need in pymysystemc_prec.h
// and not in this file

